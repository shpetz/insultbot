#guts of the insult bot

import praw

# there needs to be a settings.cfg file in the base dir for this to work.
# format:
# [auth]
# username=xxxxxxxxxxxxxxxxxx
# password=xxxxxxxxxxxxxxxxxx
def authenticate():
  config = ConfigParser.ConfigParser()
  config.read('settings.cfg')
  username = config.get('auth', 'username')
  password = config.get('auth', 'password')
  print '[*] Logging in as %s...' % username
  r.login(username, password)
  print '[*] Login successful...\n'

def main():
  USER_AGENT = '/u/insultbot by /u/Shpetz https://github.com/Shpetz/insultbot'
  r = praw.Reddit(user_agent=USER_AGENT)
  
  authenticate()
  
  posted = 0 # number of comments posted this session
  
  subreddit = dae = r.get_subreddit('insultbottest')

  for count, submission in enumerate(subreddit.get_hot(limit=None)):
    # skip the first few submissions
    if count > IGNORE_FIRST_SUBMISSIONS and submission.id not in already_done:
        already_done.add(submission.id)

        sys.stdout.write('\r[*] %d threads processed [*] %d comments posted' %
            ((count - IGNORE_FIRST_SUBMISSIONS), posted))
        sys.stdout.flush()

        # make sure i haven't posted here earlier
        already_posted = False
        for comment in submission.comments:
            if comment.author == username:
                already_posted = True

        created = datetime.fromtimestamp(submission.created_utc) # epoch to datetime
        diff = (datetime.now() - created).total_seconds() / 60 / 60 / 24 # num days
        if (not already_posted and diff >= 3 and submission.score == 0): # post is older 3 days
            # score = 0
            comment = submission.add_comment(MESSAGE)
            #print '\n[*] %s' % submission.title
            #print '\tComment: %s' % comment.permalink
            time.sleep(SLEEP_AFTER_COMMENTING) # sleep for seconds after commenting
        #time.sleep(2) # to comply with rate limit

if __name__ == '__main__':
  main()
